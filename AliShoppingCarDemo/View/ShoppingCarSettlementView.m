//
//  ShoppingCarSettlementView.m
//  AliShoppingCarDemo
//
//  Created by z on 2017/7/13.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ShoppingCarSettlementView.h"
#import "ShoppingCarHeader.h"

@interface ShoppingCarSettlementView()
@property (nonatomic,strong) UIButton *markButton;
@property (nonatomic,strong) UILabel *totalLabel;
@property (nonatomic,strong) UILabel *transportLabel;
@property (nonatomic,strong) UIButton *submitButton;
@end

@implementation ShoppingCarSettlementView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    self.backgroundColor = [UIColor whiteColor];
    
    self.markButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(markItemAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setImage:[UIImage imageNamed:@"未选中"] forState:UIControlStateNormal];
        [tmpBtn setImage:[UIImage imageNamed:@"选中"] forState:UIControlStateSelected];
        [tmpBtn setTitle:@" 全选" forState:UIControlStateNormal];
        [tmpBtn setTitleColor:ShoppingCarBlackColor forState:UIControlStateNormal];
        tmpBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        tmpBtn.backgroundColor = [UIColor clearColor];
        tmpBtn;
    });
    [self addSubview:self.markButton];

    
    self.transportLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor = ShoppingCarGrayColor;
        tmpLabel.font = [UIFont systemFontOfSize:13];
        tmpLabel.numberOfLines = 1;
        tmpLabel.textAlignment = NSTextAlignmentCenter;
        tmpLabel;
    });
    self.transportLabel.text = @"不含运费";
    [self addSubview:self.transportLabel];
    
    self.totalLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor = ShoppingCarBlackColor;
        tmpLabel.font = [UIFont systemFontOfSize:15];
        tmpLabel.numberOfLines = 1;
        tmpLabel.textAlignment = NSTextAlignmentRight;
        tmpLabel;
    });
    self.totalLabel.text = @"合计：￥ 0";
    [self addSubview:self.totalLabel];

    self.submitButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setTitle:@"结算(0)" forState:UIControlStateNormal];
        [tmpBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        tmpBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        tmpBtn.backgroundColor = [UIColor colorWithRed:255/255.0 green:120/255.0 blue:0/255.0 alpha:1];
        tmpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        tmpBtn;
    });
    [self addSubview:self.submitButton];
    
    [self.markButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(self);
        make.width.mas_equalTo(85);
    }];
    [self.submitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.mas_equalTo(self);
        make.width.mas_equalTo(106);
    }];
    
    [self.transportLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.submitButton.mas_left).offset(-8);
        make.top.bottom.mas_equalTo(self);
    }];
    
    [self.totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.greaterThanOrEqualTo(self.mas_left).offset(85);
        make.right.equalTo(self.transportLabel.mas_left).offset(8);
        make.top.bottom.mas_equalTo(self);
    }];
}

- (void)markItemAction:(UIButton *)sender {
    sender.selected = !sender.isSelected;
}

- (void)submitAction {
    
}

@end

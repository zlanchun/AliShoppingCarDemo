//
//  ShoppingInfoHeaderView.h
//  AliShoppingCarDemo
//
//  Created by z on 2017/7/12.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingInfoHeaderView : UICollectionReusableView
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) void(^editSelf)(BOOL isSelected);
@end

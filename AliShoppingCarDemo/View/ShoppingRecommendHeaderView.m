//
//  ShoppingRecommendHeaderView.m
//  AliShoppingCarDemo
//
//  Created by z on 2017/7/13.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ShoppingRecommendHeaderView.h"
#import "ShoppingCarHeader.h"
@interface ShoppingRecommendHeaderView()
@property (nonatomic,strong) UIImageView *thumbImgVIew;
@property (nonatomic,strong) UILabel *titleLabel;

@end

@implementation ShoppingRecommendHeaderView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    self.backgroundColor = [UIColor whiteColor];
    self.thumbImgVIew = ({
        UIImageView *tmpImgView = [[UIImageView alloc] init];
        tmpImgView.contentMode = UIViewContentModeScaleAspectFit;
        tmpImgView.image = [UIImage imageNamed:@""];
        tmpImgView;
    });
    [self addSubview:self.thumbImgVIew];
    self.thumbImgVIew.backgroundColor = ShoppingCarGrayColor;
    [self.thumbImgVIew mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self);
        make.left.equalTo(self.mas_left).offset(8);
        make.width.mas_equalTo(self.mas_height);
    }];
    
    self.titleLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor = ShoppingCarGrayColor;
        tmpLabel.font = [UIFont systemFontOfSize:16];
        tmpLabel.textAlignment = NSTextAlignmentLeft;
        tmpLabel;
    });
    self.titleLabel.text = @"滚局关注的“瑞士腕表“为你推荐";
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.thumbImgVIew.mas_right).offset(8);
        make.top.bottom.right.mas_equalTo(self);
    }];
}
@end

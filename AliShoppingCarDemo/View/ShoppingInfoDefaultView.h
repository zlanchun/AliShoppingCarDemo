//
//  ShoppingInfoDefaultView.h
//  AliShoppingCarDemo
//
//  Created by z on 2017/7/12.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingInfoDefaultView : UIView
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *subTitle;
@property (nonatomic,assign) CGFloat price;
@property (nonatomic,assign) NSInteger count;
@end

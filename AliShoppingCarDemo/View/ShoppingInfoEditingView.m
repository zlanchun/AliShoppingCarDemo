//
//  ShoppingInfoEditingView.m
//  AliShoppingCarDemo
//
//  Created by z on 2017/7/12.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ShoppingInfoEditingView.h"
#import "ShoppingInfoEditAmountView.h"
#import "ShoppingCarHeader.h"

@interface ShoppingInfoEditingView()
@property (nonatomic,strong) ShoppingInfoEditAmountView *amountView;
@property (nonatomic,strong) UIButton *deleteButton;
@property (nonatomic,strong) UILabel *subTitleLabel;
@property (nonatomic,strong) UIButton *downArrowButton;

@property (nonatomic, strong) MASConstraint *widthConstraint;
@end

@implementation ShoppingInfoEditingView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
        [self blockAction];
    }
    return self;
}

- (void)setupUI {
    self.amountView = [ShoppingInfoEditAmountView new];
    self.amountView.borderColor = [UIColor clearColor];
    [self addSubview:self.amountView];
    
    self.deleteButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setTitle:@"删除" forState:UIControlStateNormal];
        [tmpBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        tmpBtn.titleLabel.font = [UIFont systemFontOfSize:18];
        tmpBtn.backgroundColor = [UIColor colorWithRed:255/255.0 green:58/255.0 blue:58/255.0 alpha:1];
        tmpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        tmpBtn;
    });
    [self addSubview:self.deleteButton];
    
    [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.right.mas_equalTo(self);
        make.width.mas_equalTo(55);
    }];
    [self.amountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(10);
        make.right.equalTo(self.deleteButton.mas_left).offset(-10);
        make.left.equalTo(self.mas_left).offset(8);
        make.height.mas_equalTo(24);
    }];

    self.downArrowButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(downArrowAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setImage:[UIImage imageNamed:@"向下"] forState:UIControlStateNormal];
        tmpBtn.backgroundColor = [UIColor whiteColor ];
        tmpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        tmpBtn;
    });
    [self addSubview:self.downArrowButton];
    [self.downArrowButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(-20);
        make.right.equalTo(self.deleteButton.mas_left).offset(-10);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    self.subTitleLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor =  ShoppingCarGrayColor;
        tmpLabel.font = [UIFont systemFontOfSize:13];
        tmpLabel.numberOfLines = 1;
        tmpLabel.textAlignment = NSTextAlignmentLeft;
        tmpLabel;
    });
    self.subTitleLabel.text = @"黑、白、美黑；36";
    [self addSubview:self.subTitleLabel];
    [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(8);
        make.bottom.equalTo(self.mas_bottom).offset(-20);
        make.right.equalTo(self.downArrowButton.mas_left).offset(-2);
        make.height.mas_equalTo(22);
    }];
}

- (void)blockAction {
    self.amountView.max = 200;
    self.amountView.changeShoppingCount = ^(ShoppingCountChangeStyle style, NSInteger currentValue) {
        NSLog(@"%ld-%ld",(long)style,currentValue);
    };
}

#pragma mark - event response

- (void)deleteAction:(UIButton *)sender {
    
}

- (void)downArrowAction:(UIButton *)sender {

}

- (void)setIsShowEditButton:(BOOL)isShowEditButton {
    _isShowEditButton = isShowEditButton;
    if (isShowEditButton) {
        [self.deleteButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.right.mas_equalTo(self);
            make.width.mas_equalTo(55);
        }];
    } else {
        [self.deleteButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.right.mas_equalTo(self);
            make.width.mas_equalTo(0);
        }];
    }
}
#pragma mark - private method


@end

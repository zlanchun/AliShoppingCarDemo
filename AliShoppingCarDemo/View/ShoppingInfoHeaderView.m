//
//  ShoppingInfoHeaderView.m
//  AliShoppingCarDemo
//
//  Created by z on 2017/7/12.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ShoppingInfoHeaderView.h"
#import "ShoppingCarHeader.h"

@interface ShoppingInfoHeaderView()
@property (nonatomic,strong) UIButton *markButton;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UIButton *editButton;
@end

@implementation ShoppingInfoHeaderView
- (void)prepareForReuse {
    [super prepareForReuse];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    self.backgroundColor = [UIColor whiteColor];
    self.markButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(markItemAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setImage:[UIImage imageNamed:@"未选中"] forState:UIControlStateNormal];
        [tmpBtn setImage:[UIImage imageNamed:@"选中"] forState:UIControlStateSelected];
        tmpBtn.backgroundColor = [UIColor clearColor];
        tmpBtn;
    });
    [self addSubview:self.markButton];
    [self.markButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(self);
        make.width.mas_equalTo(40);
    }];
    
    self.titleLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor = ShoppingCarBlackColor;
        tmpLabel.font = [UIFont systemFontOfSize:15];
        tmpLabel.numberOfLines = 2;
        tmpLabel.textAlignment = NSTextAlignmentLeft;
        tmpLabel;
    });
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(40);
        make.right.equalTo(self.mas_right).offset(-15);
        make.top.bottom.mas_equalTo(self);
    }];
    
    self.editButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setTitle:@"编辑" forState:UIControlStateNormal];
        [tmpBtn setTitle:@"完成" forState:UIControlStateSelected];
        [tmpBtn setTitleColor:ShoppingCarBlackColor forState:UIControlStateNormal];
        tmpBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        tmpBtn.backgroundColor = [UIColor whiteColor ];
        tmpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        tmpBtn;
    });
    [self addSubview:self.editButton];
    [self.editButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-12);
        make.bottom.top.mas_equalTo(self);
    }];
    
    
    UIView *separatorView = [UIView new];
    separatorView.backgroundColor = ShoppingCarGrayColor;
    [self addSubview:separatorView];
    [separatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.bottom.right.mas_equalTo(self);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)markItemAction:(UIButton *)sender {
    sender.selected = !sender.isSelected;
}

- (void)editAction:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    if  (self.editSelf) {
        self.editSelf(sender.selected);
    }
}

- (void)setTitle:(NSString *)title {
    _title = [title copy];
    self.titleLabel.text = _title;
}

@end

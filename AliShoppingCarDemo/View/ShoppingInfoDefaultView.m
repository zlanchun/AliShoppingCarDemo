//
//  ShoppingInfoDefaultView.m
//  AliShoppingCarDemo
//
//  Created by z on 2017/7/12.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ShoppingInfoDefaultView.h"
#import "ShoppingCarHeader.h"

@interface ShoppingInfoDefaultView()
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *subTitleLabel;
@property (nonatomic,strong) UILabel *priceLabel;
@property (nonatomic,strong) UILabel *countLabel;
@end

@implementation ShoppingInfoDefaultView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    self.backgroundColor = [UIColor whiteColor];
    self.titleLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor = ShoppingCarBlackColor;
        tmpLabel.font = [UIFont systemFontOfSize:15];
        tmpLabel.numberOfLines = 2;
        tmpLabel.textAlignment = NSTextAlignmentLeft;
        tmpLabel;
    });
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(8);
        make.top.mas_equalTo(self);
        make.right.equalTo(self.mas_right).offset(-15);
        make.height.mas_equalTo(37);
    }];
    
    self.subTitleLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor =  ShoppingCarGrayColor;
        tmpLabel.font = [UIFont systemFontOfSize:13];
        tmpLabel.numberOfLines = 1;
        tmpLabel.textAlignment = NSTextAlignmentLeft;
        tmpLabel;
    });
    [self addSubview:self.subTitleLabel];
    [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(8);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
        make.right.equalTo(self.mas_right).offset(-15);
        make.height.mas_equalTo(15);
    }];
    
    self.priceLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor =  ShoppingCarRedColor;
        tmpLabel.font = [UIFont systemFontOfSize:11];
        tmpLabel.numberOfLines = 1;
        tmpLabel.textAlignment = NSTextAlignmentLeft;
        tmpLabel;
    });
    [self addSubview:self.priceLabel];
    
    self.countLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor =  ShoppingCarGrayColor;
        tmpLabel.font = [UIFont systemFontOfSize:13];
        tmpLabel.numberOfLines = 1;
        tmpLabel.textAlignment = NSTextAlignmentRight;
        tmpLabel;
    });
    [self addSubview:self.countLabel];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(8);
        make.right.mas_greaterThanOrEqualTo(self.countLabel.mas_left).offset(2);
        make.bottom.equalTo(self.mas_bottom);
        make.height.mas_equalTo(15);
    }];
    
    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-15);
        make.bottom.equalTo(self.mas_bottom);
        make.height.mas_equalTo(15);
    }];
}

- (void)setTitle:(NSString *)title {
    _title = [title copy];
    self.titleLabel.text = _title;
}
-(void)setSubTitle:(NSString *)subTitle {
    _subTitle = [subTitle copy];
    self.subTitleLabel.text = _subTitle;
}
- (void)setPrice:(CGFloat)price {
    _price = price;
    self.priceLabel.text = [NSString stringWithFormat:@"￥%.0f",price];
}
- (void)setCount:(NSInteger)count {
    _count = count;
    self.countLabel.text = [NSString stringWithFormat:@"x%ld",count];
}
@end

//
//  ShoppingInfoEditAmountView.h
//  ShoppingCar
//
//  Created by z on 2017/7/11.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,ShoppingCountChangeStyle) {
    ShoppingCountDecrease = 0,  /**< 减少1个商品 */
    ShoppingCountIncrease = 1,  /**< 增加1个商品 */
    ShoppingCountChangedByTextfield = 2 /**< 修改该商品数量 */
};

/**
 * height: 22 其他的没要求
*/
@interface ShoppingInfoEditAmountView : UIView

/// default: #d5d5d5
@property (nonatomic,strong) UIColor *borderColor;
/// default: 0.8f
@property (nonatomic,assign) CGFloat borderWidth;
/// default: 3.0f
@property (nonatomic,assign) CGFloat cornerRadius;
/// default: ∞，最大值数量，即加按钮能达到的最大数值。
@property (nonatomic,assign) NSUInteger max;
/// 修改商品数量
@property (nonatomic,copy) void(^changeShoppingCount)(ShoppingCountChangeStyle style,NSInteger currentValue);

/// 取消编辑状态
- (void)endEditing;
@end

//
//  ShoppingInfoCell.h
//  AliShoppingCarDemo
//
//  Created by z on 2017/7/12.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,ShoppingInfoCellState) {
    ShoppingInfoCellDefault,
    ShoppingInfoCellSelfEditing,
    ShoppingInfoCellAllEditing
};

@interface ShoppingInfoCell : UICollectionViewCell
@property (nonatomic,assign) ShoppingInfoCellState state;
@end

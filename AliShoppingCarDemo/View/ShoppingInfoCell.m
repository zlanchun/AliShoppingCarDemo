//
//  ShoppingInfoCell.m
//  AliShoppingCarDemo
//
//  Created by z on 2017/7/12.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ShoppingInfoCell.h"
#import "ShoppingInfoEditAmountView.h"
#import "ShoppingCarHeader.h"
#import "ShoppingInfoDefaultView.h"
#import "ShoppingInfoEditingView.h"

@interface ShoppingInfoCell()
@property (nonatomic,strong) UIButton *markButton;
@property (nonatomic,strong) UIImageView *coverImgView;
@property (nonatomic,strong) ShoppingInfoDefaultView *defaultView;
@property (nonatomic,strong) ShoppingInfoEditingView *editingView;
@end

@implementation ShoppingInfoCell

#pragma mark - life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    self.backgroundColor = [UIColor whiteColor];
    
    self.markButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(markItemAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setImage:[UIImage imageNamed:@"未选中"] forState:UIControlStateNormal];
        [tmpBtn setImage:[UIImage imageNamed:@"选中"] forState:UIControlStateSelected];
        tmpBtn.backgroundColor = [UIColor whiteColor];
        tmpBtn;
    });
    [self addSubview:self.markButton];
    [self.markButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(self);
        make.width.mas_equalTo(40);
    }];
    
    self.coverImgView = ({
        UIImageView *tmpImgView = [[UIImageView alloc] init];
        tmpImgView.contentMode = UIViewContentModeScaleAspectFill;
        tmpImgView.image = [UIImage imageNamed:@""];
        tmpImgView.layer.cornerRadius = 5;
        tmpImgView.layer.borderWidth = 0.6;
        tmpImgView.layer.borderColor = [UIColor colorWithRed:213/255.0 green:213/255.0 blue:213/255.0 alpha:1].CGColor;
        tmpImgView;
    });
    self.coverImgView.backgroundColor = [UIColor lightTextColor];
    [self addSubview:self.coverImgView];
    [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(40);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(95, 95));
    }];
    
    self.defaultView = [ShoppingInfoDefaultView new];
    [self addSubview:self.defaultView];
    [self.defaultView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self.coverImgView);
        make.left.equalTo(self.coverImgView.mas_right);
        make.right.equalTo(self.mas_right).offset(0);
    }];
    
    self.defaultView.title = @"Nike耐克女鞋2017夏季运动鞋Air Max全掌气垫减震跑步鞋";
    self.defaultView.subTitle = @"黑、白、美黑；36";
    self.defaultView.price = 1036;
    self.defaultView.count = 1;
    
    self.editingView = [ShoppingInfoEditingView new];
    self.editingView.hidden = YES;
    [self addSubview:self.editingView];
    [self.editingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.right.mas_equalTo(self);
        make.left.equalTo(self.coverImgView.mas_right);
    }];
}

#pragma mark - private methods


#pragma mark - event response
- (void)markItemAction:(UIButton *)sender {
    sender.selected = !sender.isSelected;
}

- (void)setState:(ShoppingInfoCellState)state {
    _state = state;
    switch (state) {
        case ShoppingInfoCellDefault: {
            self.defaultView.hidden = NO;
            self.editingView.hidden = YES;
        }
            break;
        case ShoppingInfoCellSelfEditing: {
            self.defaultView.hidden = YES;
            self.editingView.hidden = NO;
            self.editingView.isShowEditButton = YES;
        }
            break;
        case ShoppingInfoCellAllEditing: {
            self.defaultView.hidden = YES;
            self.editingView.hidden = NO;
            self.editingView.isShowEditButton = NO;
        }
            break;
        default:
            break;
    }
}

@end

//
//  ShoppingCarHeader.h
//  ShoppingCar
//
//  Created by z on 2017/7/11.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#ifndef ShoppingCarHeader_h
#define ShoppingCarHeader_h

#import <Masonry/Masonry.h>

#define ShoppingCarBlackColor [UIColor colorWithRed:60/255.0 green:59/255.0 blue:63/255.0 alpha:1]
#define ShoppingCarGrayColor  [UIColor colorWithRed:213/255.0 green:213/255.0 blue:213/255.0 alpha:1]
#define ShoppingCarRedColor [UIColor colorWithRed:250/255.0 green:47/255.0 blue:56/255.0 alpha:1]


#endif /* ShoppingCarHeader_h */

//
//  ShoppingInfoEditingView.h
//  AliShoppingCarDemo
//
//  Created by z on 2017/7/12.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingInfoEditingView : UIView
@property (nonatomic,assign) BOOL isShowEditButton;
@end

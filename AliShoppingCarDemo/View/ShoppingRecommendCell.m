//
//  ShoppingRecommendCell.m
//  ShoppingCar
//
//  Created by z on 2017/7/12.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ShoppingRecommendCell.h"
#import "ShoppingCarHeader.h"
#import "NSString+MoneyStyle.h"

@interface ShoppingRecommendCell()
@property (nonatomic,strong) UIImageView *coverImgView;
@property (nonatomic,strong) UILabel *titleLabel;

@property (nonatomic,strong) UILabel *priceLabel;
@property (nonatomic,strong) UIButton *buyButton;
@end

@implementation ShoppingRecommendCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
        [self setMoneyStyleWithMoney:100.23];
    }
    return self;
}

// 宽高比：0.64
- (void)setupUI {
    self.layer.borderWidth = 0.8;
    self.layer.borderColor = ShoppingCarGrayColor.CGColor;
    
    self.coverImgView = ({
        UIImageView *tmpImgView = [[UIImageView alloc] init];
        tmpImgView.contentMode = UIViewContentModeCenter;
        tmpImgView;
    });
    self.coverImgView.backgroundColor = ShoppingCarGrayColor;
    [self addSubview:self.coverImgView];
    [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(self);
        make.height.mas_equalTo(self.bounds.size.width);
    }];
    
    self.titleLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor = ShoppingCarBlackColor;
        tmpLabel.font = [UIFont systemFontOfSize:16];
        tmpLabel.numberOfLines = 1;
        tmpLabel.textAlignment = NSTextAlignmentLeft;
        tmpLabel;
    });
    self.titleLabel.text = @"ck石英k3m21126男表";
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.coverImgView.mas_bottom).offset(6);
        make.left.equalTo(self.mas_left).offset(8);
        make.right.equalTo(self.mas_right).offset(-8);
        make.height.mas_equalTo(36);
    }];
    
    self.priceLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor = ShoppingCarRedColor;
        tmpLabel.font = [UIFont systemFontOfSize:16];
        tmpLabel.textAlignment = NSTextAlignmentCenter;
        tmpLabel;
    });
    self.priceLabel.text = @"￥100";
    [self addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(-4);
        make.left.equalTo(self.mas_left).offset(8);
        make.height.mas_equalTo(24);
    }];
    
    self.buyButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(buyAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setImage:[UIImage imageNamed:@"购物车"] forState:UIControlStateNormal];
        tmpBtn.layer.borderColor = ShoppingCarGrayColor.CGColor;
        tmpBtn.layer.borderWidth = 0.5f;
        tmpBtn.layer.cornerRadius = 3.0f;
        tmpBtn.backgroundColor = [UIColor clearColor ];
        tmpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        tmpBtn;
    });
    [self addSubview:self.buyButton];
    [self.buyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(-4);
        make.right.equalTo(self.mas_right).offset(-4);
        make.width.height.mas_equalTo(24);
    }];
}

- (void)setMoneyStyleWithMoney:(CGFloat)money {
    self.priceLabel.attributedText = [NSString differentFontWithMoney:money moneyFont:[UIFont systemFontOfSize:13] integerFont:[UIFont systemFontOfSize:15] decimalPointFont:[UIFont systemFontOfSize:11]];
}

- (void)buyAction:(UIButton *)sender {
    
}
@end

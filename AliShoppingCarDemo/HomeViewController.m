//
//  HomeViewController.m
//  AliShoppingCarDemo
//
//  Created by z on 2017/7/12.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "HomeViewController.h"
#import "ShoppingInfoCell.h"
#import "ShoppingInfoHeaderView.h"
#import "ShoppingRecommendCell.h"
#import "ShoppingRecommendHeaderView.h"
#import "ShoppingCarSettlementView.h"

typedef NS_ENUM(NSInteger,ShoppingCarSectionType) {
    ShoppingCarInfo = 1,
    ShoppingCarRecommend
};

@interface HomeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,assign) ShoppingInfoCellState state;
@property (nonatomic,strong) ShoppingCarSettlementView *settlementView;
@property (nonatomic,strong) NSDictionary *sectionDictionary;
@end

@implementation HomeViewController

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"TaoBaoShoppingCarDemo";
    self.view.backgroundColor = [UIColor whiteColor];
    self.sectionDictionary = @{ @(0):@(ShoppingCarInfo),
                                @(1):@(ShoppingCarInfo),
                                @(2):@(ShoppingCarRecommend),
                                @(3):@(ShoppingCarRecommend) };
    [self configureNav];
    [self configureCollectionView];
    [self configureSettlementView];
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.sectionDictionary.allKeys.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
    cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *result = self.sectionDictionary[@(indexPath.section)];
    switch (result.integerValue) {
        case ShoppingCarInfo: {
            ShoppingInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShoppingInfoCell" forIndexPath:indexPath];
            cell.state = self.state;
            return cell;
        }
        case ShoppingCarRecommend: {
            ShoppingRecommendCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShoppingRecommendCell" forIndexPath:indexPath];
            return cell;
        }
        default: {
            ShoppingRecommendCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShoppingRecommendCell" forIndexPath:indexPath];
            return cell;
        }
    }
    
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *result = self.sectionDictionary[@(indexPath.section)];
    switch (result.integerValue) {
        case ShoppingCarInfo: {
            ShoppingInfoHeaderView *infoHeaderView = (ShoppingInfoHeaderView *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ShoppingInfoHeaderView" forIndexPath:indexPath];
            infoHeaderView.title = @"test";
            __weak typeof(self) weakSelf = self;
            infoHeaderView.editSelf = ^(BOOL isSelected) {
                if (isSelected) {
                    NSLog(@"进入编辑状态");
                    weakSelf.state = ShoppingInfoCellSelfEditing;
                } else {
                    weakSelf.state = ShoppingInfoCellDefault;
                }
                [weakSelf.collectionView reloadData];
            };
            return infoHeaderView;
        }
        case ShoppingCarRecommend: {
            ShoppingRecommendHeaderView *recommendHeaderView = (ShoppingRecommendHeaderView *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ShoppingRecommendHeaderView" forIndexPath:indexPath];
            return recommendHeaderView;
        }
        default: {
            ShoppingRecommendHeaderView *recommendHeaderView = (ShoppingRecommendHeaderView *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ShoppingRecommendHeaderView" forIndexPath:indexPath];
            return recommendHeaderView;
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *result = self.sectionDictionary[@(indexPath.section)];
    switch (result.integerValue) {
        case ShoppingCarInfo: {
            CGSize size =  CGSizeMake([UIScreen mainScreen].bounds.size.width, 104);
            return size;

        }
        case ShoppingCarRecommend: {
            CGFloat width = [UIScreen mainScreen].bounds.size.width;
            CGSize size =  CGSizeMake((width - 6 * 1) / 2.0, ((width - 6 * 1) / 2.0)/0.7115);
            return size;
        }
        default: {
            CGFloat width = [UIScreen mainScreen].bounds.size.width;
            CGSize size =  CGSizeMake((width - 6 * 1) / 2.0, ((width - 6 * 1) / 2.0)/0.7115);
            return size;
        }
    }
}

//这个方法是返回 Header的大小 size
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 44);
}


#pragma mark - CustomDelegate

#pragma mark - event response
- (void)configureNav {
    UIButton *rightButton = ({
        UIButton *tmpBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
        [tmpBtn addTarget:self action:@selector(rightAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setTitle:@"编辑" forState:UIControlStateNormal];
        [tmpBtn setTitle:@"完成" forState:UIControlStateSelected];
        [tmpBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        tmpBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        tmpBtn.backgroundColor = [UIColor clearColor ];
        tmpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        tmpBtn;
    });
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
}

- (void)configureSettlementView {
    [self.view addSubview:self.settlementView];
    [self.view bringSubviewToFront:self.settlementView];
    self.settlementView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 50 - 49, [UIScreen mainScreen].bounds.size.width, 50);
}

- (void)configureCollectionView {
    self.collectionView.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1];
    [self.view addSubview:self.collectionView];
    self.collectionView.alwaysBounceVertical = YES;
    
    [self.collectionView registerClass:[ShoppingInfoCell class] forCellWithReuseIdentifier:@"ShoppingInfoCell"];
    [self.collectionView registerClass:[ShoppingRecommendCell class] forCellWithReuseIdentifier:@"ShoppingRecommendCell"];
    [self.collectionView registerClass:[ShoppingInfoHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ShoppingInfoHeaderView"];
    [self.collectionView registerClass:[ShoppingRecommendHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ShoppingRecommendHeaderView"];

    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

- (void)rightAction:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    if  (sender.isSelected) {
        self.state = ShoppingInfoCellAllEditing;
    } else {
        self.state = ShoppingInfoCellDefault;
    }
    [self.collectionView reloadData];
}
#pragma mark - private methods

#pragma mark - getters and setters
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        CGFloat space = 0;
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        flowLayout.minimumLineSpacing = space;
        flowLayout.minimumInteritemSpacing = space;
        flowLayout.sectionInset = UIEdgeInsetsMake(space, 0, 10, 0);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    }
    return _collectionView;
}

- (ShoppingCarSettlementView *)settlementView {
    if (!_settlementView) {
        _settlementView = [ShoppingCarSettlementView new];
    }
    return _settlementView;
}
#pragma mark - receive memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
